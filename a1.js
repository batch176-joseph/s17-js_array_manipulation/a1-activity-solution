let students = []

/////////////////////ADD-STUDENT\\\\\\\\\\\\\\\\\\\\\\\

//Create an addStudent() function that will accept a name of the student and add it to the student array.

function addStudent(studentName){
	console.log(`${studentName} was added to the student's list`);
	students.push(studentName);
	students.sort();
	}



/////////////////////COUNT-STUDENT\\\\\\\\\\\\\\\\\\\\\\\

//Create a countStudents() function that will print the total number of students in the array.

function countStudents(){
	console.log(`There are a total of ${students.length} students enrolled`);

	}



//XXXXXXXXXXXXXXXXXXXXPRINT-STUDENTXXXXXXXXXXXXXXXXXXXX

//Create a printStudents() function that will sort and individually print the students (sort and forEach methods) in the array.

let sortStudents = students.sort();

//The Function
function printStudents(){
		students.forEach(function(sortStudents){
			console.log(sortStudents);
		})

	}



//XXXXXXXXXXXXXXXXXXXFIND-STUDENTXXXXXXXXXXXXXXXXXXXXXXX

/*Create a findStudent() function that will do the following:
a. Search for a student name when a keyword is given (filter method).
b. If one match is found print the message studentName is an enrollee.
c. If multiple matches are found print the message studentNames are enrollees.
d. If no match is found print the message studentName is not an enrollee.
e. The keyword given should not be case sensitive.
*/ 

//The Idea
/*let filteredStudents = students.filter(function(students){
		return students.toLowerCase().includes(`joe`);
	})

console.log(filteredStudents);
console.log(filteredStudents.length);
*/



//The Function

function findStudent(x){
let filteredStudents = students.filter(function(filteredStudents){
	return filteredStudents.toLowerCase().includes(x.toLowerCase())
	})
	
	if (filteredStudents.length == 1){
		console.log(`${filteredStudents} is an enrollee`)
	} else if (filteredStudents.length >= 2){
		console.log(`${filteredStudents.join(`, `)} are enrollees`)
	} else if (filteredStudents.length <= 0){
		console.log(`No student found with the name ${x}`)

}
}


/////////**************STRETCH GOALS******************



//XXXXXXXXXXXXXXXXXXXXXADD-SECTIONXXXXXXXXXXXXXXXXXXX

//Create an addSection() function that will add a section to all students in the array with the format of studentName - Section A (map method).

function addSection(){
	let sectionStudent = students.map(function(students){
		return students + ` - Section A`
	})
}



//XXXXXXXXXXXXXXXXXXXREMOVE-STUDENTXXXXXXXXXXXXXXXXXXXX

/*Create a removeStudent() function that will do the following: 
a. Capitalize the first letter of the user's input (toUpperCase and slice methods.
b. Retrieve the index of the student to be removed (indexOf method).
c. Remove the student from the array (splice method).
d. Print a message that the studentName was removed from the student's list.
*/

function removeStudent(){

}
